# Deploy to AWS with GitLab CI/CD

GitLab provides Docker images with the libraries and tools you need to deploy to AWS. 

Read more about AWS deployment features here https://docs.gitlab.com/ee/ci/cloud_deployment.

[[_TOC_]]

## EC2 components

### ec2-provision-and-deploy

An all-in-one component that [provisions the infrastructure](#provisioning) and deploys to EC2 for both [review](#ec2-deploy-review) and [production](#ec2-deploy-production).

```yaml
include:
  - component: gitlab.com/components/aws/ec2-provision-and-deploy@<VERSION>
    inputs:
      stage: build
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `provision_stage`  |  | The pipeline stage where to add the CloudFormation provision job |
| `review_stage`     |  | The pipeline stage where to add the review deployment job |
| `production_stage` |  | The pipeline stage where to add the production deployment job |

### provisioning

Provision the infrastructure using AWS CloudFormation API.

```yaml
include:
  - component: gitlab.com/components/aws/provision@<VERSION>
    inputs:
      stage: build
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` |  | The pipeline stage where to add the CloudFormation provision job |
| `registry_host` | `$CI_TEMPLATE_REGISTRY_HOST` | The GitLab registry host to use to pull the container image |

### ec2-deploy-review

Deploy to EC2 to a specific review environment.

By default the deployment to review environment is executed on merge request pipelines and tag pipelines.

```yaml
include:
  - component: gitlab.com/components/aws/ec2-deploy-review@<VERSION>
    inputs:
      stage: deploy
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` |  | The pipeline stage where to add the review deployment job |
| `environment` | `review/$CI_COMMIT_REF_NAME` | The environment name where to perform the deployment |
| `registry_host` | `$CI_TEMPLATE_REGISTRY_HOST` | The GitLab registry host to use to pull the container image |

### ec2-deploy-production

Deploy to EC2 to a production environment.

It by default deploys to a production environment for tags and the default branch. 

```yaml
include:
  - component: gitlab.com/components/aws/ec2-deploy-production@<VERSION>
    inputs:
      stage: deploy
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` |  | The pipeline stage where to add the production deployment job |
| `environment` | `review/$CI_COMMIT_REF_NAME` | The environment name where to perform the deployment |
| `registry_host` | `$CI_TEMPLATE_REGISTRY_HOST` | The GitLab registry host to use to pull the container image |

## ECS and Fargate components

Components related to Amazon Elastic Container Service and Fargate.

### ecs-deploy-review

Deploy to ECS to a specific review environment.

By default the deployment to review environment is executed on merge request pipelines and tag pipelines.

```yaml
include:
  - component: gitlab.com/components/aws/ecs-deploy-review@<VERSION>
    inputs:
      deploy_stage: deploy
      cleanup_stage: deploy
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `deploy_stage` |  | Stage where the review deployment will run |
| `cleanup_stage` |  | Stage where the job that stops the review environment will run |
| `environment` | `review/$CI_COMMIT_REF_NAME` | The environment name where to perform the deployment |

### ecs-deploy-production

Deploy to ECS to a production environment.

It by default deploys to a production environment for tags and the default branch. 

```yaml
include:
  - component: gitlab.com/components/aws/ecs-deploy-production@<VERSION>
    inputs:
      stage: deploy
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` |  | The pipeline stage where to add the production deployment job |
| `environment` | `review/$CI_COMMIT_REF_NAME` | The environment name where to perform the deployment |

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 
